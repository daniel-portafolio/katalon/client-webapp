import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.pURL)

WebUI.click(findTestObject('Object Repository/Page_Infosys/i_Home_zmdi zmdi-menu (2)'))

WebUI.click(findTestObject('Object Repository/Page_Infosys/a_Admin (1)'))

WebUI.click(findTestObject('Object Repository/Page_Infosys/a_Clients (1)'))

WebUI.click(findTestObject('Object Repository/Page_Infosys/a_Add Client (1)'))

WebUI.waitForElementVisible(findTestObject('Page_Infosys/input_First Name_firstName'), 2)

WebUI.setText(findTestObject('Object Repository/Page_Infosys/input_First Name_firstName'), Nombre)

WebUI.setText(findTestObject('Object Repository/Page_Infosys/input_Last Name_lastName (1)'), Apellidos)

WebUI.click(findTestObject('Object Repository/Page_Infosys/div_Address'))

WebUI.setText(findTestObject('Object Repository/Page_Infosys/input_Address_txtAddress (1)'), Direccion)

WebUI.setText(findTestObject('Object Repository/Page_Infosys/input_Age_txtAge (1)'), Edad)

WebUI.setText(findTestObject('Object Repository/Page_Infosys/input_Email address_exampleInp (1)'), correo)

WebUI.click(findTestObject('Page_Infosys/btn_crear'))

WebUI.closeBrowser()

